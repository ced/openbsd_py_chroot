#!/usr/bin/env python
"""
Create a python environment in a chroot filesystem
"""

import os
import re
import shutil
import subprocess
import sys
import time

CHROOT_TARGET = "/opt/www"

def allow_dev_nodes():
    "Change mount options for root of CHROOT_TARGET, to allow device node creation"

    root_of_chroot = "/".join(CHROOT_TARGET.split("/")[:2])

    mount_conf = "/etc/fstab"
    backup_file = mount_conf + ".bak-" + str(int(time.time()))
    print("Backing up {0} to {1}".format(mount_conf, backup_file))
    shutil.copy2(mount_conf, backup_file)
    mount_conf_orig = []
    with open(mount_conf, "r") as m_fh:
        mount_conf_orig = m_fh.read().splitlines()

    print("Updating {0}".format(mount_conf))
    mount_conf_out = open(mount_conf, "w")
    for line in mount_conf_orig:
        # Test for line
        could_be_dev = re.match(r"^[\w\/]", line)
        # Mounts have fields:
        # fs_spec fs_file fs_vfstype [fs_mntopts fs_freq fs_passno]
        fields = line.split(maxsplit=5)
        if (not could_be_dev
            or len(fields) < 4
            or fields[1] != root_of_chroot):
            mount_conf_out.write(line + "\n")
            continue

        # We could do a line.replace() operation here instead, but I feel safer
        # when specifically targetting the fs_mntopts field of the line.
        fs_mntopts = fields[3].split(",")
        if not "nodev" in fs_mntopts:
            mount_conf_out.write(line + "\n")
        else:
            new_opts = [o for o in fs_mntopts if o != "nodev"]
            if not 'wxallowed' in new_opts:
                new_opts.append('wxallowed')
            new_fields = fields[:3] + [",".join(new_opts)]
            if len(fields) >= 4:
                new_fields += fields[4:]
            new_line = " ".join(new_fields)
            mount_conf_out.write(new_line + "\n")
    mount_conf_out.close()
    print("Remounting {0} to allow dev nodes".format(root_of_chroot))
    output = subprocess.check_output(["mount"])
    nice_output = "\n".join([l.decode(encoding="UTF-8") for l in output.splitlines()])
    print(nice_output)
    subprocess.check_call(["mount", "-u", "-o", "dev", root_of_chroot])
    output = subprocess.check_output(["mount"])
    nice_output = "\n".join([l.decode(encoding="UTF-8") for l in output.splitlines()])
    print(nice_output)

def chroot_dev_nodes():
    "Create chroot dev nodes"

    chroot_dev = os.path.join(CHROOT_TARGET, "dev")
    if not os.path.isdir(chroot_dev):
        os.makedirs(chroot_dev)
    # Format is: (mode, name, device_type, major, minor)
    nodes = (("644", "urandom", "c", "45", "2"),
             ("666", "null", "c", "2", "2"),
             ("666", "zero", "c", "2", "12"),
             ("644", "random", "c", "45", "0"),
             ("644", "srandom", "c", "45", "1"),
             ("644", "arandom", "c", "45", "3"),
            )
    for node_conf in nodes:
        target_node = os.path.join(chroot_dev, node_conf[1])
        if os.path.exists(target_node):
            print("node {0} already exists".format(target_node))
            continue
        print("creating node {0}".format(target_node))
        subprocess.check_call(["mknod", "-m"] + list(node_conf), cwd=chroot_dev)

def chroot_skel_dirs():
    # <chroot>/run is our slowcgi socket target
    dirs = ("dev", "usr/bin", "sbin", "run", "var/run")
    for base_dir in dirs:
        os.makedirs(os.path.join(CHROOT_TARGET, base_dir), exist_ok=True)

def copy_into_chroot(files):
    for path in files:
        base_path, name = os.path.split(path)
        dest_dir = CHROOT_TARGET + base_path
        dest = os.path.join(dest_dir, name)
        if not os.path.isdir(dest_dir):
            print("creating {0}".format(dest_dir))
            os.makedirs(dest_dir)
        print("copying {0} to {1}".format(path, dest))
        shutil.copy2(path, dest)

def copy_chroot_utils():
    utils = ("/usr/bin/env", "/sbin/ldconfig", "/usr/bin/ldd")
    copy_into_chroot(utils)

def slowcgi_setup():
    "Set the slowcgi config in /etc/rc.conf.local"

    slowcgi_config = 'slowcgi_flags="-p {chroot} -s {socket}"'
    rc_conf = "/etc/rc.conf.local"
    backup_file = rc_conf + ".bak-" + str(int(time.time()))
    print("Backing up {0} to {1}".format(rc_conf, backup_file))
    shutil.copy2(rc_conf, backup_file)
    rc_conf_orig = []
    with open(rc_conf, "r") as rc_fh:
        rc_conf_orig = rc_fh.read().splitlines()

    slowcgi_socket_path = os.path.join(CHROOT_TARGET, "run")
    slowcgi_socket_file = os.path.join(slowcgi_socket_path, "slowcgi.sock")

    rc_conf_out = open(rc_conf, "w")
    for line in rc_conf_orig:
        if not line.startswith("slowcgi_flags="):
            rc_conf_out.write(line + "\n")
        else:
            rc_conf_out.write(slowcgi_config.format(chroot=CHROOT_TARGET, socket=slowcgi_socket_file) + "\n")
    rc_conf_out.close()

def ldd(exe):
    "Return list of what executable is linked to"
    try:
        output = subprocess.check_output(["ldd", exe])
    except subprocess.CalledProcessError as err:
        # This can happen if the file isn't an ELF executable
        return []
    else:
        return [l.split(maxsplit=6)[-1].decode(encoding="UTF-8") for l in output.splitlines()[2:]]

def chroot_py_skel_dirs(linked_to):
    "Create skeleton dirs for stuff that python is linked to"
    linked_paths = [os.path.dirname(l) for l in linked_to]
    for base_dir in sorted(set(linked_paths)):
        # os.path.join() doesn't like two strings starting with "/"
        skel_dir = CHROOT_TARGET + base_dir
        print("creating {0}".format(skel_dir))
        os.makedirs(skel_dir, exist_ok=True)

def py_core_deps():
    "Return a list of the things that python core libraries link to"
    deps = []
    for dyn_path in [p for p in sys.path if p.endswith("lib-dynload")]:
        for root, dirs, files in os.walk(dyn_path):
            [deps.extend(ldd(os.path.join(root, f))) for f in files]
    return sorted(set(deps))

def py_env_files():
    py_files = []
    for path in [p for p in sys.path if p.startswith("/usr")]:
        for root, dirs, files in os.walk(path):
            py_files.extend([os.path.join(root, f) for f in files])
    return sorted(set(py_files))

def py_bin_files():
    bin_files = []

    if "PATH" not in os.environ:
        paths = ("/bin", "/sbin", "/usr/bin", "/usr/sbin", "/usr/local/bin", "/usr/local/sbin")
    else:
        paths = os.environ["PATH"].split(":")

    for path in [p for p in paths if os.path.isdir(p)]:
        bin_files.extend([os.path.join(path, f) for f in os.listdir(path) if f.startswith("python")])
    return sorted(set(bin_files))

def chroot_update_libcache():
    "Update chroot shared library cache"
    constant_lib_paths = ["/usr/lib", "/usr/local/lib"]
    py_paths = [p for p in sys.path if p.startswith("/usr") and os.path.isdir(p)]
    combined_paths = sorted(set(constant_lib_paths + py_paths))
    subprocess.check_call(["chroot", CHROOT_TARGET, "/sbin/ldconfig"] + combined_paths)

def display_py_posixpath_notice():
    "Print a warning, that user may need to patch python posixpath.py file for chroot to work properly"
    message = """


!!!!!!!!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!!!!!!

The <chroot>/usr/local/lib/python<ver>/posixpath.py file may need patching in order to work
in a chroot environment. At time of writing, the posixpath.expanduser() function
needed to be changed like this:

     if i == 1:
-        if 'HOME' not in os.environ:
-            import pwd
-            userhome = pwd.getpwuid(os.getuid()).pw_dir
-        else:
-            userhome = os.environ['HOME']
+        try:
+            if 'HOME' not in os.environ:
+                import pwd
+                userhome = pwd.getpwuid(os.getuid()).pw_dir
+            else:
+                userhome = os.environ['HOME']
+        except:
+            userhome = "/"
     else:
         import pwd
         name = path[1:i]

Please remember to check your module to see if it needs the patch ^_^

As for httpd config, it can look like this:

chroot "/opt/www"
server "something here" {
    location "/cgi/*" {
        fastcgi socket "/run/slowcgi.sock"
    }
}
    """
    print(message)

if __name__ == "__main__":
    print("Writing slowcgi config")
    slowcgi_setup()

    print("Creating chroot skeleton dirs")
    chroot_skel_dirs()

    copy_chroot_utils()

    this_python = sys.executable
    print("this python: {0}".format(this_python))

    python_ldd = ldd(this_python)
    print("python linked to:")
    for l in python_ldd: print("\t{0}".format(l))

    print("Creating chroot python skeleton dirs")
    chroot_py_skel_dirs(python_ldd)

    print("Copying python deps into chroot")
    python_deps = sorted(set(python_ldd + py_core_deps()))
    copy_into_chroot(python_deps)

    print("Copying python env into chroot")
    python_files = py_env_files()
    copy_into_chroot(python_files)

    print("Copying python bin into chroot")
    python_bin_files = py_bin_files()
    copy_into_chroot(python_bin_files)

    print("Allowing dev node creation for chroot partition")
    allow_dev_nodes()

    print("Creating chroot dev nodes")
    chroot_dev_nodes()

    print("Updating chroot shared library cache")
    chroot_update_libcache()

    print("Restarting slowcgi")
    subprocess.check_call(["rcctl", "restart", "slowcgi"])

    display_py_posixpath_notice()
